from napalm import get_network_driver
from threading import Thread
import csv
import pandas as pd
import os
from time import strftime
from napalm_dbase import queryinput			#this is the database querying program which is sent to you by name 'napalm_dbase.py'
import time
import subprocess
import diffios
import platform

#DO NOT start reading this code from here, the parts of code where it starts with 'def' are all functions and will be coming into picture only when we call them.
#this is a function by name 'abstracted' where we connect to devices and send abstracted napalm commands to them and get outputs
def abstracted(input,command):
	#getting the 'napalm' network driver based on the type of 'OS' given by the user
	driver = get_network_driver(str(input['OS']))
	try:
		device = driver(str(input['SSHIP']),str(input['username']),str(input['pass']))		#connecting to device via ssh with details provided by user
		device.open()		#opening the device
		print("Connection to device "+str(input['Devicename'])+" is successful")
	except:
		#printing below statement if in case we are unable to connect to the device using ssh
		print("Connection to device "+str(input['Devicename'])+" failed")
		quit()
	#checking whether user is requesting for getting running-configuration
	if command == 'get_config()':
		#in this case we are diverting code to function by name 'configuration'
		configuration(input,device,command)
	#if user is requesting to get some info but its not running-configuration though
	else:
		try:
			#this is where we are executing the command on a device, 'eval' is a function which enables us to execute a string of characters as a string
			getop = eval('device.'+str(command))
			#time.sleep enables us to make the code to go into sleep mode for some time, so that we are able to get the whole info from devices
			time.sleep(5)			#sleeping for 3 seconds
			print(getop)
			# storing the information received from the device in dictionary of dictionaries 'output', which is a global variable
			output[str(input['Devicename'])] = {}
			output[str(input['Devicename'])]['abstract'] = str(getop)
		except:
			#in case of any errors while fetching data
			print("Error while getting data from device "+str(input['Devicename']))
		#closing the ssh connection to the device
		device.close()				#!always remember to close the connections you opened!



#this is a function by name 'clithrough' where we connect to devices and send cli-commands(specific to OEM)
def clithrough(input,command,step):
	driver = get_network_driver(str(input['OS']))
	try:
		device = driver(str(input['SSHIP']),str(input['username']),str(input['pass']))		#connecting to device via ssh with details provided by user
		device.open()		#opening the device
		print("Conection to device "+str(input['Devicename'])+" is successful")
	except:
		print("Conection to device "+str(input['Devicename'])+" failed")
		quit()
	#sending cli-based command to the device, we got this command from the database file
	host = device.cli([str(command)])
	time.sleep(3)
	device.close()
	if step == '0':
		output[str(input['Devicename'])] = {}
		output[str(input['Devicename'])]['cli'] = str(host[str(command)])
	elif step == '1':
		clioutput[str(input['Devicename'])] = {}
		clioutput[str(input['Devicename'])]['cli'] = str(host[str(command)])
	
#this is a function by name 'configuration' where we get running-configurations of devices
def configuration(input,device,command):
	try:
		config = eval('device.'+str(command))
		#selecting 'running' config
		conf = config['running']
		#since running-configurations on our devices are huge, we need to wait for sometime to get it, here it is 5 seconds
		time.sleep(10)
	except:
		print("Error while grabbing running config from device "+str(input['Devicename']))
		device.close()
		quit()
	#using 'strftime' from 'time' module to get the current timestamp
	timestamp = strftime("%Y-%m-%d_%H_%M_%S")
	#naming the file where run-conf will be stored as 'Devicename_timestamp.txt'
	conffile = str(input['Devicename'])+"_"+str(timestamp)+".txt"
	#opening the txt file to write the run-conf into it
	runconf = open(conffile,"wb")
	try:
		#iterating through the run-conf obtained and writing them onto txt file just created
		for line in str(conf):
			runconf.write(line.encode('utf-8'))
		print("Running config file for "+str(input['Devicename'])+" saved successfully")
	except:
		#in case of any errors while writing to txt file
		print("Running config file for "+str(input['Devicename'])+" not saved, some issue is there")
	#closing the txt file
	runconf.close()
	device.close()
	#appending the txt file name into 'filenames' variable, this will be used in case user wants to compare between run-confs of devices
	filenames[(str(input['Devicename'])).lower()] = str(conffile)

#this is a function by name 'configuredevices' where we can change the configuration of devices by making changes
def configuredevices(input,device,conffile):
	#we are sending commands from the txt file present in the working directory to the device
	print("merge")
	try:
		device.load_merge_candidate(filename=str(conffile))
		time.sleep(2)
		device.commit_config()
		time.sleep(2)
		device.close()
		print("Configuration changes implemented succesfully for device "+str(input['Devicename'].lower()))
	except:
		print("Error occurred while configuring device "+str(input['Devicename'].lower()))
		device.close()

#this function comes into play when user wants to get information or send cli show commands only to selective devices
def getsiteinfo(reqdevices,final):
	#spliting the device list given by user using ',' as delimiter
	reqdeviceslist = reqdevices.split(',')
	reqdevs = []
	newfinal = []
	for dev in reqdeviceslist:
		validitycounter = 0
		reqdevs.append(dev.lower())
		for element in final:
			if element['Devicename'].lower() == dev.lower():
				validitycounter = 1
			else:
				continue
		if validitycounter == 0:
			print("Device "+str(dev.lower())+" is not present in the input file given by you, please check!")
	for entry in final:
		if ((entry['Devicename']).lower()) in reqdevs :
			newfinal.append(entry)
		else:
			continue
	#returning to the variable where the function is called
	return(newfinal)

#this is the function which helps us to save the details we fetched into a file
def savingdatatofile(output,destfile):
	data = pd.DataFrame(output)		#getting dataframe using pandas, in order to save in csv file
	finaldata = data.T		#transposing the dataframe produced in previous step for better display of results
	path = os.getcwd()		#getting current working directory details to save file in proper destination
	timestamp = strftime("%Y-%m-%d_%H_%M_%S")    #getting current timestamp
	savefile = destfile+"_"+str(timestamp)+".csv"		#creating the name of final file using timestamp
	savepath = os.path.join(path,savefile)		#path along with the filename, where all details from devices will be saved
	try:
		finaldata.to_csv(savepath)		#saving the data in csv file
		print("File saved successfully in "+str(savefile)+" file successfully")
	except:
		print("Error in output file saving")

#asking user for inputs via .csv file
print("**WARNING***Please make sure that all the files you give as input during the script is present in the working directory**")
filename = input('Enter the file name where you have stored the required inputs for devices: (make sure its in .csv format)\n')
#verifying whether filename mentioned by user is actually present in the working directory
if os.path.isfile(filename) == 0:
	print("File is not there in specified path, please check!!")
	exit()
#initializing the 'filenames' variable, where we will store the names of running-configuration files
global filenames
filenames = {}

#here we are opening the input file and extracting the ip,username,password,os etc

with open(filename,'r') as file:		#opening file with read permissions
	csvread = csv.DictReader(file)		#reading csv file in dictionary format
	i = 0			#initializing a counter which will be incremented in below 'for' loop
	final = []		#initializing a list by name 'final', which contains multiple dictionaries
	#iterating through the CSV file and making dictionaries for every device with all its login details
	for ele in csvread:
		dict = 'dict'+str(i)
		dict = {}				#initializing one dictionary per device in input file
		#assigning the inputs/login details to keys for each dictionary, like IP,username,password etc.
		dict['Devicename']=ele['Device']
		dict['SSHIP'] = ele['SSH IP']
		dict['OS'] = ele['OS']
		dict['username'] = ele['Username']
		dict['pass'] = ele['Password']
		final.append(dict)		#appending each and every dictionary with login details, to the list 'final'(list of dictionaries)
		i = i+1
#this is the place where we are asking user on which kind of action he wants to perform on the sites
while True:
	action = input("""Enter your requirement
    Available options are:
    Enter 1 for Ping check
    Enter 2 for getting information (includes running-config too)
    Enter 3 for comparing configuration
    Enter 4 for changing configuration
    Enter 5 for sending CLI show commands\n""")
	#verifying whether user gave a valid input, it should be from 1,2,3,4,5 only
	if action == '1' or action == '2' or action == '3' or action == '4' or action == '5':
		print("Thanks for entering a valid input!")
		break		#breaking this 'while' loop once we get a valid input from user
	#if user input is invalid then asking user to enter the input again
	else:
		print("Wrong input!! try again!")
		continue

#below is the place where code gets to if user enters '1' as action
if action == '1':
	#initializing a counter to count for how many devices the ping check has failed
	failure_counter = 0
	#iterating through the devices in the file and checking connectivity
	for input in final:
		try:
			#using subprocess to send a ping 'some ip' from your cmd prompt
			if platform.system().lower() == 'windows':
				ping = subprocess.check_output(['ping','-n','2',str(input['SSHIP'])])
			else:
				ping = subprocess.check_output(['ping', '-c', '2', str(input['SSHIP'])])
			print("ping is successful for device "+str(input['Devicename']))
		except:
			#printing that ping failed if device is not reachable, 'try' and 'except' block is used for this purpose
			print("ping failed for device "+str(input['Devicename']))
			failure_counter = failure_counter+1
	#checking 'failure_counter' variable to know how many devices failed in connectivity check
	if failure_counter == 0:
		print("Ping is successfull for all the devices")
	else:
		print("Ping failed for "+str(failure_counter)+" devices")
	
#below is the place where code gets to if user enters '2' as action
if action == '2':
	#asking the user regarding how many sites he/she wants to get information from
	reqsites = input("""Please choose one of the following:
		Enter 1 - for getting information from all devices in file
		Enter 2 - for getting information from selective devices \n""")
	#if user chooses for geetting info from selective devices then asking user to provide list of devices
	if reqsites == '2':
		reqdevices = input("Enter the site list, separated by 'comma': \n")
		#calling a function by name 'getsiteinfo' where we choose credentials only for required devices
		newfinal = getsiteinfo(reqdevices,final)
		final = newfinal			#updating the list of dictionaries ('final') with only the credentials of devices which are requested by user
	#if user chooses to go ahead with all devices
	elif reqsites == '1':
		print("You have selected to go ahead with all the devices")
	#dealing with wrong input by user
	else:
		print("Wrong input!! you should enter either '1' or '2'!")
		quit()
	# since we are using 'napalm' here we need to send an abstracted command for getting some specific info, we have a database(.db) file ready for this.
	# we need to enter the key-word from the database excel sheet provided to you
	print("""Before entering keyword here, please check whether that specific 'napalm' command mentioned in database file is supported for this OS.
	To do so you can grab the napalm command mentioned in 'dbase_xcel.xlsx' file and check it in below mentioned link:
	https://napalm.readthedocs.io/en/latest/support/index.html#general-support-matrix 
	If it is not supported for this OS type, please go for getting info through CLI command - option/action 5 \n""")
	keyword = input("Please enter the key word here: \n")
	#making sure that the key word is not running-configuration, then only we will ask for destination filename
	if keyword != 'running-configuration':
		#asking user to enter a name for the file where they want the info to be stored post fetching
		destfile = input("Enter the destination filename here: (no need to mention the format it is defaultly set to .csv)\n")
	#initializing the database file - !!DO NOT CHANGE THIS!!
	dbase = 'napalm_database.db'
	#calling the 'queryinput' function from 'napalm_dbase.py' file which is already imported into this program
	query = queryinput(dbase,keyword)
	#working on the output from dbase query we just sent
	command = query[0]			#actual napalm command from dbase file
	typeofcommand = query[1]		#'typeofcommand' is where we come to know whether its an abstracted or a cli command
	threads = []		#initializing a list to keep track of multiple threads and to join them
	global output		#Making the final output dictionary of dictionaries a global variable
	output = {}
	#this is the place where we use multithreading and try to call 'abstracted' or 'clithrough' function based on 'typeofcommand' variable
	for input in final:
		if typeofcommand == 'abstract':
			#initializing a thread targetted towards 'abstracted' function with required arguments
			sshtodevice = Thread(target=abstracted,args=[input,command])
		elif typeofcommand == 'cli':
			#this step variable is just to differentiate between user requesting via keyword or directly with cli command
			step = '0'
			# initializing a thread targetted towards 'clithrough' function with required arguments
			sshtodevice = Thread(target=clithrough,args=[input,command,step])
		sshtodevice.start()		#starting the thread
		threads.append(sshtodevice)		#appending current thread to threads list
	#Joining the threads
	for thread in threads:
		thread.join()
	#calling 'savingdatatofile' function if keyword is not equal to 'running-configuration'
	if keyword != 'running-configuration':
		savingdatatofile(output,destfile)
		
#below is the place where code gets to if user enters '3' as action
if action == '3':
	#asking user input on how many sites they want to compare
	countofsites = input("How many sites do you want to compare? - please enter numerical value only!\n")
	#asking user to enter the device name, which will act as baseline for the comparision
	basesite = input("Enter the device which is base for comparision here: \n")
	#asking user for other device names which will be compared with baseline device
	othersites = input("Enter the other sites, separated by comma : \n")
	#spliting the 'othersites' and storing them in a list by name 'sites'
	sites = []
	sites.append(basesite)
	otherlist = othersites.split(',')
	sites = sites+otherlist
	#'credentials' is the new list of dictionaries which have credentials for user requested sites
	credentials = []
	#'newsites' is the list of devicenames after converting their names into lowercase, for uniformity
	newsites = []
	for site in sites:
		newsites.append(site.lower())
	#entering credentials into 'credentials' list
	for dev in final:
		if dev['Devicename'].lower() in newsites:
			credentials.append(dev)
		else:
			continue
	#first we need to get the running-configs of all devices mentioned by user
	command = "get_config()"
	threads = []

	for input in credentials:
		driver = get_network_driver(str(input['OS']))
		try:
			device = driver(str(input['SSHIP']),str(input['username']),str(input['pass']))		#connecting to device via ssh with details provided by user
			device.open()		#opening the device
			print("Conection to device "+str(input['Devicename'])+" is successful")
		except:
			print("Conection to device "+str(input['Devicename'])+" failed")
		func = Thread(target=configuration,args=[input,device,command])
		func.start()
		threads.append(func)

	for thread in threads:
		thread.join()
	baseline = filenames[basesite.lower()]
	#this is the part of the code where we compare between two .txt files, which contain the running=-configs
	for site in newsites:
		if site == basesite.lower():
			continue
		else:
			comparision = filenames[site.lower()]
			diff = diffios.Compare(baseline,comparision)
			data = diff.delta()
			timestamp = strftime("%Y-%m-%d_%H_%M_%S")  # getting current timestamp
			savefile = str(basesite.lower())+"VS"+str(site.lower())+"_" + str(timestamp) + ".txt"
			conf = open(savefile, "w+")
			for line in data:
				conf.write(line)
			print("Differences between requested devices is saved in file "+str(savefile)+" successfully")

#below is the place where code gets to if user enters '4' as action
if action == '4':
	#warning to user giving the information about the requirements before proceeding
	print("""Before proceeding make sure the files with commands/configuration changes for thes sites are in below mentioned format:
	'device_name_conf.txt' -- everything in lower case please!
	If not get these files and try again""")
	#requesting count and devicenames for which user want to implement
	count = input("How many sites do you want to implement? \n")
	sites = input("Enter the sites for which you want to change configuraion, separated by comma : \n")
	sitelist = sites.split(",")
	conffilelist = {}
	credentials = []
	#checking for 'devicename_conf.txt' files in the working directory
	for site in sitelist:
		conffile = str(site.lower())+"_conf.txt"
		conffilelist[str(site.lower())] = str(conffile)
		if os.path.isfile(conffile) == 0:
			print("configuration file for "+str(site.lower())+"  is missing or some mistake in naming") 
			print("""please check and try again! Filenames should be in below format:
			'device_name_conf.txt' -- everything in lower case please!""")
			exit()
	newsites = []
	for site in sitelist:
		newsites.append(site.lower())
	for dev in final:
		if dev['Devicename'].lower() in newsites:
			credentials.append(dev)
		else:
			continue
	threads = []
	for input in credentials:
		driver = get_network_driver(str(input['OS']))
		try:
			device = driver(str(input['SSHIP']),str(input['username']),str(input['pass']))		#connecting to device via ssh with details provided by user
			device.open()		#opening the device
			print("Conection to device "+str(input['Devicename'])+" is successful")
		except:
			print("Conection to device "+str(input['Devicename'])+" failed")
		func = Thread(target=configuredevices,args=[input,device,conffilelist[(str(input['Devicename']).lower())]])
		func.start()
		threads.append(func)
	for thread in threads:
		thread.join()	

#below is the place where code gets to if user enters '5' as action
if action == '5':
	#requesting user to enter the choice of devices
	print("!!Note that this option is only to send 'show' commands, you cannot use this to send global config commands!!")
	reqclisites = input("""Please choose one of the following:
		Enter 1 - for sending a CLI command to all devices in file
		Enter 2 - for sending a CLI command to selective devices \n""")
	#if user wants to go with selective devices, asking him/her to enter the device list
	if reqclisites == '2':
		reqclidevices = input("Enter the device list, separated by 'comma': \n")
		newfinal = getsiteinfo(reqclidevices,final)
		final = newfinal
	elif reqclisites == '1':
		print("You have choosen to go ahead with all the devices")
	else:
		print("Wrong input!! you should enter either '1' or '2'!")
		quit()
	#initializing a global variable by name 'clioutput' where details for all devices are stored and used to store them in a file
	global clioutput
	clioutput = {}
	#asking user for the destination filename
	destclifile = input("Enter the destination filename here: (no need to mention the format it is defaultly set to .csv)\n")
	#asking user for the cli command he/she wants to send to devices
	command = input("Please enter your CLI command here (Do not use this for getting running configs!!) \n")
	step = '1'		#this 'step' variable is just to differentiate between user requesting via keyword or directly with cli command
	threads = []
	for input in final:
		sshtodevice = Thread(target=clithrough,args=[input,command,step])
		sshtodevice.start()		#starting the thread
		threads.append(sshtodevice)		#appending current thread to threads list

	for thread in threads:
		thread.join()		#joining threads
	#saving the results into a file
	savingdatatofile(clioutput,destclifile)


	
